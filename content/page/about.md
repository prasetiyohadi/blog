+++
title = "About this blog"
date = 2014-08-08T20:53:07.573882-04:00
author = "Prasetiyo Hadi Purwoko"
description = "Things about me."
+++

## About

This is the Hugo code I use to generate the HTML files of my blog. A devops/sysadmin blog about web and network technology stacks, also provides tutorials and original writing of the author. This blog also covers exciting news in science and technology.

## Powered by

[Hugo](https://gohugo.io/) is a general-purpose website framework. Technically speaking, Hugo is a static site generator. Unlike systems that dynamically build a page with each visitor request, Hugo builds pages when you create or update your content. Since websites are viewed far more often than they are edited, Hugo is designed to provide an optimal viewing experience for your website’s end users and an ideal writing experience for website authors.

[ghostwriter](https://github.com/jbub/ghostwriter) is a fork of simple AJAX driven theme for the Ghost blogging platform ported to Hugo.
