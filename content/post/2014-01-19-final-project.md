---
layout: single
title: "Final Project"
date: 2014-01-19 07:00:00 +0700
categories: university
comments: true
---

I have reached the eight semester in my undergraduate study and it means this semester I have to finish a final project (Tugas Akhir, ID) as a requirement to graduate. Actually I don't really into my final project's topic. There are many factors behind it. In the new curriculum, my department decide to give a final project to a team consist of two students. This is a good news because the possibility of student who will graduate is increased. But, this is somewhat a setback for me. I don't usually work in a team and for my final project I want to do a final project which is a original, only for me, my own final project. The requirement that a final project must be done by a team makes me don't feel that this is really my own final project.

I often consider myself as a "lone wolf" and my skills and interests differ far from those in my department. For example, when most of students in my department have interest in control and instrumentation engineering or building engineering, my interest is in computation and materials engineering. Fortunately, I still like to get along well with my friends and I still can consider myself as an "activist" in my campus although my skills make me more suitable as nerd, and yes, I am starting to think that I am more nerd right now.

###My department?
My department is Engineering Physics, a cool name, isn't it? But, I still don't know for clear what is the goal and the purpose of my own department. Some say that the core of my department is control and instrumentation engineering or building engineering. Some say that the core of my department is to measure or in instrumentation. Actually those definitions is a total demotivation because I think I do not fall into group of people who think like that.

But, I got some enlightenment this morning from one of my teacher. His opinion in my department makes me understand what role my department holds and my skill is not a waste in my department. They talk about control, instrumentation, or anything but the real thing is beyond that. We exist because world needs some guys who can control the physics phenomenons so people from other departments can use the phenomenons in their field effectively and in the process of controlling those phenomenons there is an act of measuring, instrumentation. Moreover, my department also involved in designing the whole system, we exist in every part in the system which interact with physical world.

Say some people from materials engineering want to process a material into something. For that purpose they need a spesific environment (temperature, pressure, etc.) or specific material. Who is responsible in creating such environment or preparing those specific (or special) material? Us. Say people from mining engineering want to explore new resources using ground signal sensor. Who is responsible in providing the sensor which suits the needs of people in mining engineering? Us. We are responsible, because if it's not us, who else will?

###My lab?
My lab is Computational Materials Design, you hear the name. Maybe you think that this lab is suitable for me, isn't. Not really, I still do not really into this. I don't know why, this lab is my dream. But, maybe this is because the setbacks from my final project earlier. This lab is more research than engineering, actually. Here we calculate to define the properties of material we set in specific configuration and because of that this lab is somehow not really get along with the purpose of my department. Although my department needs this lab.

This lab's work is to compute and for that it needs computer, a lot of computers, with high performance. But, for this small lab, only a few of HPC (High Performance Computer) available, and sadly, not configured optimally. I am just newbie here so I still don't really know how this lab works and what will I really do. But I can say that the people in this lab is amazing. Their works are amazing too. They often go to abroad to attend international conference or something and get a chance to meet people from abroad. They often get offering to continue their study in Japan, Europe, or USA.

But still, it is really hard to start working. As the last person who enter this lab for this period, I often wonder how can I close the gap in knowledge and skill between mine and people who enters first. Moreover, I already have a job, and a hobby, in network engineering and administration. Although that boost my potential in computation field, it's no use if I don't know the theory and I do not like to be left behind. So, rather than struggling to study, I prefer to find a new field. So, my job now become my fall back, when I feel not motivated to study the theory. Honestly, this is what I feel now.

###Now, what?
I don't know. Tomorrow I have to give a presentation on my final project's progress, when there is still no progress at all. This is only me anyway, I am the lazy one. Or maybe tonight I will try something new, trying to fing my dying motivation and revive it again. I think this is some kind of curse because I often compare myself with people around me and think low of myself.

But, someday I also want to say out loud that I am proud of what I really am. Not what people think of I am.
