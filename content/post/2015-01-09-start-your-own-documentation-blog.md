---
layout: single
title: "Start Your Own Documentation Blog"
date: 2015-01-09 07:00:00
categories: sysadmin
comments: true
---

I am trying to write again after some month was absent from touching any electronic notes (blogs). Actually I wanted to do it from a long time ago but I was afraid don't have any material with qualities to be written. In reality, I realized that I have been encountering events that I must write about. After so long trying to make up my mind and gather my courage, I finally be able to put some ideas into my writing, starting with this re-introduction.

This blog actually planned to be a place where I keep my documentation about thing that I have been thinkering with. But I think I was too strict. Why don't I write anything I want to write here? This is my *personal* blog after all. So, I think it is wiser to share any good things with folks from internet here. As a system administrator which also want to be a developer, I think having a blog to write about your project or anything you get in touch is essential. It keeps a record about your projects and also can be a source of help for someone working the same projects out there.

Okay, I think this is all from me now. We will catch up again real soon, I hope.
