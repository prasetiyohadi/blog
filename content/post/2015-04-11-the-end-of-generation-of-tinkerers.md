---
layout: single
title: "The End of Generation of Tinkerers"
date: 2015-04-11 07:00:00
categories: idea
comments: true
---

#### The Prologue
My college was the only college I know that gave the chance to its students to run the college network infrastructures. It encourages the students to study more outside their respective fields and share the knowledge in a community. Only a handful of people know that in the early days, the network infrastructure of my college, was built through the hands of its own students and the tradition to maintain it has been passed through the new generation of students.

The community of students which involved in maintaining the campus network gets benefits like faster network access, special routes and proxies, and access to computers across campus networks, as the compensations of the time and energy they used for maintaining campus network infrastructures. The benefits of maintaining the campus network infrastructures, despite only obtained by a few students, has been the driving force of the community to keep tinkering with the campus networks and develop theirs and others community, also to study new technology or trends like Internet of Things, which will be too expensive for common students.

#### The Present
The network infrastructures has been evolved faster and faster by the time. Now we face terms like Software Defined Network, OpenStack, DevOps, and many more terms. The amount of knowledge and requirements needed to run the network infrastructures has grown beyond the capability of the campus students, some said. So, in the wake of new form of our college department which runs the campus network infrastructures, they decide to cut ties of any students involvement in operations of campus network infrastructures.

The new directorate of campus network operations wants to control the network operations from the backbone until the user access, simply everything. The new directorate demands that people who runs the campus network infrastructures to work full time, so no students allowed, despite there are a few part time students which runs the most important parts of campus networks, they kick them out one by one.

Now, the community of students which involved in running the campus network in the past facing an uncertain future. When they are not involved in running the campus networks, their existence is no more relevant than any other students communities which just a bunch of students with same hobby, or culture, or whatever ties them as a community. The new directorate offers them to do projects with strict contracts for the directorate for the substitution of running campus networks.

#### The Opinion
Before I walked out from the campus (not dropped out apparently, of course I graduated), the campus security unit has been implementing a new protocol, the so called night hours for the students, to keep them out of campus from 11 p.m. until the next morning. Of course the students communities became upset, their activity hours had been cut significantly. One of the community which greatly affected with this new protocol is our community which involved in running campus network. We usually start tinkering from the evening after courses until the next morning. But, when the new _security_ protocol implemented, its only give the students approximately four hours effective for the activity.

Of course we cannot cope with the demand of the technology which grows faster and faster when our time to study it has been cut more than sixty percent a day. I know only a very few students has the dedication to do the jobs, but they are the tinkerers, they are the ones who inspire the others to study the technology and develop it. They are the ones who tell the other students that our campus is different than other campuses, that there are another ways than to give up to education industry scheme, that the access to technology not only limited to those who have the money. We do not have any money, but we never afraid to tinker anything because we have access, and now they want to cut it. 

I know it is no place for me, who had been out of the campus, to say those words. But, I don't want something that makes me very proud with my college gets taken away just like that, and frankly I don't need MIT if my college still supports those few students which are willing to give their time and energy so they can tinker the technology beyond their reach and I am very disappointed to see my college downgrades to the level of college with access only for the rich people.
